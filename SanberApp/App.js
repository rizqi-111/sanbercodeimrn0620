import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tugas from './Tugas/Tugas12/App'
import Video from './Tugas/Tugas12/components/videoItem'

// export default function App() {
  const App = () => {
  return (
    <Tugas />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default App;