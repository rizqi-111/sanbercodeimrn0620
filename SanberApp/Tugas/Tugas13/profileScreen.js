import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

let win = Dimensions.get('window') ;
export default class profileScreen extends Component {
  render (){
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
          <Text style={styles.title}>My Profile</Text>
      </View>
      <View style={styles.body}>
        <Icon2 name="person" size={300}></Icon2>
      </View>
      <View style={styles.about}>
        <Text style={styles.titleAbout}>About Me :</Text>
        <Text style={styles.subTitle}>Hi, Ahmad Rizqi</Text>
        <Text style={styles.subTitle}>Your Specialist : Web Developer</Text>
      </View>
      <View style={styles.find}>
        <Text style={styles.titleAbout}>Find Me :</Text>
        <View style={styles.logo}>
          <Ionicons style={styles.navItem} name="logo-facebook" size={50}></Ionicons>
          <Ionicons style={styles.navItem} name="logo-instagram" size={50}></Ionicons>
          <FontAwesome name="gitlab" size={50} color="black" />
          <Ionicons style={styles.navItem} name="logo-github" size={50}></Ionicons>
          <Ionicons style={styles.navItem} name="logo-twitter" size={50}></Ionicons>
        </View>
      </View>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  navBar: {
    marginTop:25,
    height : 55,
    width: win.width,
    backgroundColor: "#FAFFFB",
    elevation: 3,
    paddingHorizontal:15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'      
  },
  title: {
      fontWeight: 'bold',
      fontSize: 18
  },
  titleAbout: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 15
  },
  subTitle: {
      fontSize: 14,
      alignSelf: 'flex-start',
      marginLeft: 30,
      marginTop: 50
  },
  rightNav: {
    flexDirection: 'row'
  },
  body: {
    flex: 1,
    width: win.width,
    height: 190,
    backgroundColor: '#C4C4C4',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'  
  },
  about: {
      flex: 1,
      width: win.width,
      height: 190,
      backgroundColor: '#FAFFFB',
      flexDirection: 'column',
      alignItems: 'center'
  },
  find: {
    flex: 1,
    width: win.width,
    height: 190,
    backgroundColor: '#45D1A0',
    flexDirection: 'column',
    alignItems: 'center'
  },
  logo: {
      width: win.width,
      height: 100,
      marginTop: 50,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center'
  }
});