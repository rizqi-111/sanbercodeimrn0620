import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SignIn, CreateAccount, Search, Home, Details, Search2, Profile} from './Screen';
import {createDrawerNavigator} from '@react-navigation/drawer';


const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () => {
  
    return (
      <HomeStack.Navigator>
        <HomeStack.Screen name="Home" component={Home}></HomeStack.Screen>
        <HomeStack.Screen name="Details" component={Details} 
        options ={({ route }) => ({
          title: route.params.name
        })}></HomeStack.Screen>
      </HomeStack.Navigator>
    )
  
}

const SearchStackScreen = () => {
  
    return (
      <SearchStack.Navigator>
        <SearchStack.Screen name="Search" component={Search}></SearchStack.Screen>
        <SearchStack.Screen name="Search2" component={Search2}></SearchStack.Screen>
      </SearchStack.Navigator>
    )
  
}

const ProfileStack = createStackNavigator();
const ProfileStackscreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}></ProfileStack.Screen>
  </ProfileStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen}></Tabs.Screen>
    <Tabs.Screen name="Search" component={SearchStackScreen}></Tabs.Screen>
  </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();

export default () => (
  // render(){
    
      <NavigationContainer>
        <Drawer.Navigator>
          <Drawer.Screen name="Home" component={TabsScreen}></Drawer.Screen>
          <Drawer.Screen name="Profile" component={ProfileStackscreen}></Drawer.Screen>
        </Drawer.Navigator>
        {/* <AuthStack.Navigator>
          <AuthStack.Screen name="SignIn" component={SignIn} options={{ title: 'Sign In'}}></AuthStack.Screen>
          <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title:'Create Account'}}></AuthStack.Screen>
        </AuthStack.Navigator> */}
      </NavigationContainer>
    
  // }
);