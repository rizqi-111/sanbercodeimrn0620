import React, { Component } from 'react';
import {
  StyleSheet, 
  Text, 
  View
} from 'react-native';

export const ProjectScreen = () => (
    
    <View style={styles.container}>
        <Text>Halaman Proyek</Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});