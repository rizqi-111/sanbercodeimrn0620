import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions
} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';

let win = Dimensions.get('window') ;
// export default class profileScreen extends Component {
export const DashboardScreen = () => ( 
  // render (){
  // return (
    <View style={styles.container}>
      <View style={styles.navBar}>
          <Text style={styles.title}>Dashboard</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.greetBox}>
            <Text style={styles.greet}>Hi, Ahmad Rizqi</Text>
            <Text style={styles.profile}>My Profile</Text>
        </View>
        <View style={styles.box}>
            <Text style={styles.titleBox}>Programming Language</Text>
            <View style={styles.item}>
                <View style={styles.itemImage}>
                    <FontAwesome5 name="php" size={40} color="black" />
                </View>
                <View style={styles.itemContent}>
                    <Text>Advance PHP</Text>
                    <Text>88 %</Text>
                </View>
            </View>
            <View style={styles.item}>
                <View style={styles.itemImage}>
                    <FontAwesome5 name="python" size={40} color="black" />
                </View>
                <View style={styles.itemContent}>
                    <Text>Intermediate Python</Text>
                    <Text>50 %</Text>
                </View>
            </View>
        </View>
        <View style={styles.box}>
            <Text style={styles.titleBox}>Framework / Library</Text>
            <View style={styles.item}>
                <View style={styles.itemImage}>
                    <FontAwesome5 name="fire" size={40} color="black" />
                </View>
                <View style={styles.itemContent}>
                    <Text>Intermediate Code Igniter</Text>
                    <Text>60 %</Text>
                </View>
            </View>
            <View style={styles.item}>
                <View style={styles.itemImage}>
                    <FontAwesome5 name="react" size={40} color="black" />
                </View>
                <View style={styles.itemContent}>
                    <Text>Basic React Native</Text>
                    <Text>5 %</Text>
                </View>
            </View>
        </View>
        <View style={styles.box}>
            <Text style={styles.titleBox}>Technology</Text>
            <View style={styles.item}>
                <View style={styles.itemImage}>
                    <FontAwesome5 name="gitlab" size={40} color="black" />
                </View>
                <View style={styles.itemContent}>
                    <Text>Basic Gitlab</Text>
                    <Text>20 %</Text>
                </View>
            </View>
        </View>
      </View>
    </View>
  // );
  // }
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  navBar: {
    // marginTop:25,
    height : 55,
    width: win.width,
    backgroundColor: "#FAFFFB",
    elevation: 3,
    paddingHorizontal:15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'      
  },
  
  body: {
    flex: 1
  },
  greetBox: {
    width: win.width,
    height: 85,
    flexDirection: 'row',
    // alignItems: 'center',
    paddingHorizontal:15,
    justifyContent: "space-between"
  },
  greet: {
    alignSelf:'flex-start',
    fontWeight: 'bold', 
    marginTop: 30, 
    fontSize: 18,
    // marginRight: 5
  },
  profile: {
    color: '#02BCBF',
    fontWeight: 'bold',
    marginTop: 30, 
    // marginLeft: 5
  },
  box: {
      width: win.width - 50,
      height: 200,
      alignSelf: 'center', 
      marginBottom: 20
  },
  titleBox: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#C4C4C4',
      marginTop: 20,
      marginLeft: 10
  }, 
  item: {
    marginLeft: 40,
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  }, 
  itemImage: {
      width: 55,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center'
  },
  itemContent: {
      marginLeft: 20,
      fontSize: 14,
      width: 200,
      height: 50,
      justifyContent: 'center'
  }
});