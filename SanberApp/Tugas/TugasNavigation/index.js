import React from "react";
import {createStackNavigator,TransitionPresets} from '@react-navigation/stack';
import {LoginScreen} from './loginScreen';
import { NavigationContainer } from "@react-navigation/native";
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { DashboardScreen } from "./dashboardScreen";
import {ProfileScreen} from './profileScreen';
import {ProjectScreen} from './ProjectScreen';
import {AddScreen} from './AddScreen';

const LoginStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const DashboardStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const AuthStack = createStackNavigator();

const DashboardStackScreen = () => (
  <DashboardStack.Navigator>
    <DashboardStack.Screen name="Skill" component={DashboardScreen}></DashboardStack.Screen>
  </DashboardStack.Navigator>
)

const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={ProfileScreen}></ProfileStack.Screen>
  </ProfileStack.Navigator>
)

const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={ProjectScreen}></ProjectStack.Screen>
  </ProjectStack.Navigator>
)

const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={AddScreen}></AddStack.Screen>
  </AddStack.Navigator>
)

const DrawerScreen = ({}) => {
  return (
  <Drawer.Navigator>
    <Drawer.Screen name="Skill" component={TabsScreen}></Drawer.Screen>
    <Drawer.Screen name="About" component={ProfileStackScreen}></Drawer.Screen>
  </Drawer.Navigator>
  )
}

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={DashboardStackScreen}></Tabs.Screen>
    <Tabs.Screen name="Project" component={ProjectStackScreen}></Tabs.Screen>
    <Tabs.Screen name="Add" component={AddStackScreen}></Tabs.Screen>
  </Tabs.Navigator>
)

function AuthStackScreen() {
  return (
    <AuthStack.Navigator
      initialRouteName="Login"
      headerMode="screen"
      screenOptions={{
        title: 'Profile',
        ...TransitionPresets.SlideFromRightIOS,
      }}
      >
      <AuthStack.Screen name ="Drawer" component={DrawerScreen}/>
      <AuthStack.Screen name="Login" component={LoginScreen}/>
    </AuthStack.Navigator>
  )
}

export default () => (
  <NavigationContainer>
    <AuthStackScreen/>
  </NavigationContainer>
);