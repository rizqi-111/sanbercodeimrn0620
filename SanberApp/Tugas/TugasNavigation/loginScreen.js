import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  Image,
  TouchableOpacity,
  FlatList, 
  TextInput, 
  ScrollView,
  Button
} from 'react-native';

const ScreenContainer = ({children}) => (
  <View style={styles.container}>{children}</View>
)
export const LoginScreen = ({navigation}) => (
    <ScreenContainer>
      
        <Image 
        style={styles.logo}
        source={require('./assets/logo.png')}
        />
        <Text style={styles.title}>Sign in to Trytobest</Text>
        <ScrollView style={styles.scroll}>
          <View style={styles.body}>
            <Text style={styles.subTitle}>Username or email addres</Text>
            <TextInput style={styles.textField} placeholder="Username or email"></TextInput>
            <Text style={styles.subTitlePass}>Password                    <Text style={styles.forgot}>Forgot Password?</Text></Text>
            <TextInput secureTextEntry={true} style={styles.textField} placeholder="Password"></TextInput>
            <View style={{width:270}}>
              <Button style={styles.button} title="Sign In"> 
                // onPress={() => navigation.navigate(DrawerScreen)}>
              </Button>            
            </View>
          </View>
          <View style={styles.tabBar}>
            <Text>Don't have an account <Text style={styles.forgot}>Create an account</Text></Text>
          </View>
          <View style={styles.tabBar}>
            <Text style={styles.rule}>Term</Text>
            <Text style={styles.rule}>Privacy</Text>
            <Text style={styles.rule}>Security</Text>
            <Text style={styles.rule}>Contact</Text>
          </View>
        </ScrollView>
      
    </ScreenContainer>
  );

const styles = StyleSheet.create({
  logo: {
    marginTop: 50,
    width: 54,
    height: 54
  },
  title: {
    paddingTop: 10,
    fontSize: 14
  },
  subTitle: {
    alignSelf:'flex-start',
    fontWeight: 'bold', 
    marginTop: 20,
    marginBottom: 20
  },
  subTitlePass: {
    alignSelf:'flex-start',
    fontWeight: 'bold', 
    marginBottom: 20
  },
  forgot: {
    color: '#02BCBF',
    fontWeight: 'bold',
    marginLeft: 'auto'
  },
  rule: {
    color: '#02BCBF',
    fontWeight: 'bold'
  },
  button: {
    color: 'rgb(69, 209, 160)',
    height: 32,
    width: 270
  },
  container: {
    flex: 1,
    backgroundColor: '#FAFFFB',
    justifyContent: 'center',
    alignItems: 'center'
  },
  scroll: {
    backgroundColor: '#FAFFFB', 
    marginTop: 20
  },
  body: {
    flex:1,
    marginTop:25,
    marginBottom: 25,
    width: 305,
    height: 305, 
    backgroundColor: "#FFFFFF",
    elevation: 3,
    paddingHorizontal:15,
    flexDirection: 'column',
    alignItems: 'center'
  },
  textField: {
    width: 270,
    height: 32,
    alignSelf: 'center',
    borderTopWidth: 0.4,
    borderBottomWidth: 0.4,
    borderLeftWidth: 0.4,
    borderRightWidth: 0.4,
    backgroundColor: 'white',
    marginBottom: 30
  },
  tabBar: {
    marginBottom: 25,
    backgroundColor: '#FFFFFF',
    height: 60,
    borderTopWidth: 0.5,
    borderTopColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: "space-around",
    alignItems: 'center'
  },
});