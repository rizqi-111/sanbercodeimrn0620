var nama = "Junaedi"
var peran = "Werewolf"

if(nama == "" && peran == ""){
	console.log("Nama harus diisi!")
} else if (nama != "" && peran == ""){
	console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
} else if (nama != "" && peran == "Penyihir"){
	console.log("Selamat datang di Dunia Werewolf, "+nama+"\nHalo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama != "" && peran == "Guard"){
	console.log("Selamat datang di Dunia Werewolf, "+nama+"\nHalo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama != "" && peran == "Werewolf"){
	console.log("Selamat datang di Dunia Werewolf, "+nama+"\nHalo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
}

console.log("\n")

var tanggal = 1
var bulan = 5
var tahun = 1945
switch (bulan) {
	case 1 : { console.log(tanggal + " Januari "+ tahun); break; }
	case 2 : { console.log(tanggal + " Februari "+ tahun); break; }
	case 3 : { console.log(tanggal + " Maret "+ tahun); break; }
	case 4 : { console.log(tanggal + " April "+ tahun); break; }
	case 5 : { console.log(tanggal + " Mei "+ tahun); break; }
	case 6 : { console.log(tanggal + " Juni "+ tahun); break; }
	case 7 : { console.log(tanggal + " Juli "+ tahun); break; }
	case 8 : { console.log(tanggal + " Agustus "+ tahun); break; }
	case 9 : { console.log(tanggal + " September "+ tahun); break; }
	case 10 : { console.log(tanggal + " Oktober "+ tahun); break; }
	case 11 : { console.log(tanggal + " November "+ tahun); break; }
	case 12 : { console.log(tanggal + " Desember "+ tahun); break; }
	default : { console.log(tanggal + " " + bulan + " " + tahun); }
}
