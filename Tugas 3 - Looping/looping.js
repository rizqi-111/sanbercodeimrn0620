//Soal 1
var var1 = 2
var var2 = 20
console.log("LOOPING PERTAMA")
while(var1 <= 20){
	console.log(var1 + " - I love coding")
	var1 = var1 + 2
} 
console.log("\nLOOPING KEDUA")
while(var2 >= 2){
	console.log(var2 + " - I will become a mobile developer")
	var2 = var2 - 2
}
console.log("\n")

//Soal 2
for (var angka = 1; angka <= 20; angka++){
	if(angka % 2 == 1){
		if(angka % 3 == 0){
			console.log(angka + " - I Love Coding")
		} else {
			console.log(angka + " - Santai")
		}
	} else if(angka % 2 == 0){
		console.log(angka + " - Berkualitas")
	}
}
console.log("\n")

//Soal 3
var string = ""
for(var i = 1; i <= 4; i++){
	for (var j = 1; j <= 8; j++) {
		string += "#" 
	}
	string += "\n"
}
console.log(string)
console.log("\n")

//Soal 4
var string2 = ""
for(var a = 1; a <= 7; a++){
	for (var b = 1; b <= a; b++) {
		string2 += "#"
	}
	string2 += "\n"
}
console.log(string2)

//Soal 5
var string3 = ""
for(var c = 1; c <= 8; c++){
	if(c % 2 == 1){
		for(var d = 1; d <= 4; d++){
			string3 += " #"
		}
	} else {
		for(var e = 1; e <= 4; e++){
			string3 += "# "
		}
	}
	string3 += "\n"
}
console.log(string3)