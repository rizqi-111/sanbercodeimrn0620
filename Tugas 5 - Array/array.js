//Soal 1
function range(startNum, finishNum){
	if(startNum && finishNum){
		var arr = []

		if(startNum > finishNum){
			for (var i = startNum; i >= finishNum; i--) {
				arr.push(i)
			}
		} else if(startNum < finishNum){
			for (var j = startNum; j <= finishNum; j++) {
				arr.push(j)
			}
		} else { //startNum == finishNum
			arr.push(startnum)
		}

		return arr
	}
	else {
		return -1
	}
}
console.log("Soal 1")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("\nSoal 2")
//Soal 2
function rangeWithStep(startNum, finishNum, step){
	if(startNum && finishNum){
		var arr = []

		if(startNum > finishNum){
			var i = startNum
			while(i >= finishNum){
				arr.push(i)
				i -= step
			}
		} else if(startNum < finishNum){
			var i = startNum
			while(i <= finishNum){
				arr.push(i)
				i += step
			}
		} else { //startNum == finishNum
			arr.push(startnum)
		}

		return arr
	}
	else {
		return -1
	}
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\nSoal 3")
//Soal 3
function sum(startNum, finishNum, step = 1){
	var sum = 0
	if(startNum){
		if(startNum > finishNum){
			var i = startNum
			while(i >= finishNum){
				sum += i
				i -= step
			}
		} else if(startNum < finishNum){
			var i = startNum
			while(i <= finishNum){
				sum += i
				i += step
			}
		} else { //startNum == finishNum
			sum += startNum
		}
	}

	return sum
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\nSoal 4")
//Soal 4
function dataHandling(arr){
	for(var i = 0; i < arr.length; i++){
		console.log("Nomor ID: " + arr[i][0])
		console.log("Nama Lengkap: " + arr[i][1])
		console.log("TTL: " + arr[i][2] + " " + arr[i][3])
		console.log("Hobi: " + arr[i][4] + "\n")
	}
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

dataHandling(input)

console.log("Soal 5")
//Soal 5
function balikKata(str){
	var str2 = []

	for(var i = 0; i < str.length; i++){
		str2.unshift(str[i])
	}
	return str2.join("")
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log("\n")
//Soal 6
console.log("Soal 6")
function dataHandling2(arr){
	arr.splice(1,1,"Roman Alamsyah Elsharawy")
	arr.splice(2,1,"Provinsi Bandar Lampung")
	arr.splice(5,0,"Pria", "SMA Internasional Metro")
	console.log(arr)
	// ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]

	arrM = arr[3].split("/")
	switch(arrM[1]){
		case "01" : { var bulan = "Januari "; break; }
		case "02" : { var bulan = "Februari "; break; }
		case "03" : { var bulan = "Maret "; break; }
		case "04" : { var bulan = "April "; break; }
		case "05" : { var bulan = "Mei "; break; }
		case "06" : { var bulan = "Juni "; break; }
		case "07" : { var bulan = "Juli "; break; }
		case "08" : { var bulan = "Agustus "; break; }
		case "09" : { var bulan = "September "; break; }
		case "10" : { var bulan = "Oktober "; break; }
		case "11" : { var bulan = "November "; break; }
		case "12" : { var bulan = "Desember "; break; }
		default : { var bulan = "##"; }
	}
	console.log(bulan)

	arrM.sort(function (value1, value2) { return value2 - value1 } )
	console.log(arrM)

	arrJ = arr[3].split("/")
	console.log(arrJ.join("-"))

	console.log(arr[1].slice(0,15))
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 