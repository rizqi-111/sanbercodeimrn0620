console.log("Soal 1")
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    // Code di sini 
    if(arr){
        var obj = {}
        for(var i = 0; i < arr.length; i++){    
            obj.firstName = arr[i][0]
            obj.lastName = arr[i][1]
            obj.gender = arr[i][2]
            if(arr[i][3]){
                if(thisYear < arr[i][3]){
                    obj.age = "Invalid Birth Year"
                } else {
                    obj.age = thisYear - arr[i][3]
                }
            } else {
                obj.age = "Invalid Birth Year"
            }
            console.log(i+1 + ". " + obj.firstName + " " + obj.lastName + ": ")
            console.log(obj)
            console.log("\n")

            // console.log(i+1 + ". " + obj.firstName + " " + obj.lastName + ": { \n" + 
            //     "firstName: " + obj.firstName +
            //     "lastName: " + obj.lastName +
            //     "gender: " + obj.gender +
            //     "age: " + obj.age +
            //     "\n}\n")
        }
    } else {
        console.log("")
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("Soal 2")
function shoppingTime(memberId, money = 0) {
  // you can only write your code here!
  var list = [ ["Sepatu brand Stacattu",1500000],
  ["Baju brand Zoro",500000],
  ["Baju brand H&N",250000],
  ["Sweater brand Uniklooh",175000],
  ["Casing Handphone",50000] ]
  var objShop = {}
  var j = 0
  if(memberId){
    if(money < list[4][1]){
        return "Mohon maaf, uang tidak cukup"
    } else {
        objShop.memberId = memberId
        objShop.money = money
        objShop.listPurchased = []
        while(money >= 0 && j < 5){
            if(money >= list[j][1]){
                money -= list[j][1]
                objShop.listPurchased.push(list[j][0])
            }
            j++
        }
        objShop.changeMoney = money
        return objShop
    }
  } else {
     return "Mohon maaf, toko X hanya berlaku untuk member saja"
  }
  
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("\n Soal 3")
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  result = []
  
  bayar = 0
  if(arrPenumpang.length > 0){
    for(var i = 0; i < arrPenumpang.length; i++){
        objAngkot = {}
        objAngkot.penumpang = arrPenumpang[i][0]
        objAngkot.naikDari = arrPenumpang[i][1]
        objAngkot.tujuan = arrPenumpang[i][2]
        bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
        objAngkot.bayar = bayar
        result.push(objAngkot)
    }
  } 
  return result
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]