console.log("\nSoal 1")
console.log("\nRelease 0")
class Animal {
    // Code class di sini
    constructor(name){
    	this.name = name
    	this.legs = 4
    	this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\nRelease 1")
// Code class Ape dan class Frog di sini
class Ape extends Animal {
	constructor(name){
		super(name)
		this.legs = 2
	}
	yell(){
		console.log("Auooo")
	}
}

class Frog extends Animal {
	constructor(name){
		super(name)
	}
	jump(){
		console.log("hop hop")
	}
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("\nSoal 2")
class Clock {
    // Code di sini
    constructor({template}){
    	this.temp = template
    }

    render(){
    	this.date = new Date()

	    this.hours = this.date.getHours()
	    if (this.hours < 10) this.hours = '0' + this.hours

	    this.mins = this.date.getMinutes()
	    if (this.mins < 10) this.mins = '0' + this.mins

	    this.secs = this.date.getSeconds()
	    if (this.secs < 10) this.secs = '0' + this.secs

	    this.output = this.temp.toString()
	      .replace('h', this.hours)
	      .replace('m', this.mins)
	      .replace('s', this.secs);
	    console.log(this.output)
    }	

    start(){
    	this.timer = setInterval(() => this.render(),1000)
    }

    stop(){
    	clearInterval(this.timer)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  