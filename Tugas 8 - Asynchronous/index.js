// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function read(time,book){
	if(time == 0 || book.length == 0){
		console.log("Buku Habis")
	} else {
		readBooks(time,book.shift(),function(time2){
			return read(time2,book)
		})
	}
}

read(10000,books)