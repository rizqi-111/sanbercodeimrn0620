var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function read(time,book){
	if(time == 0 || book.length == 0){
		console.log("Buku Habis")
	} else {
		readBooksPromise(time,book.shift())
			.then(function (time1){
				read(time1,book)
			})
			.catch(function (end){
				console.log("Waktu Habis, Tersisa = "+end)
			}) 	
	}
}

read(10000,books)