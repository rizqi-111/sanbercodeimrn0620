console.log("Soal 1")
// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
 
// golden()

const golden = () => {
	console.log("this is golden!!")	
}
golden()

console.log("\nSoal 2")
// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//       // return 
//     }
//   }
// }
 
const newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
		fullName: () => {
			console.log(firstName + " " + lastName)
		}
	}
}

//Driver Code 
newFunction("William", "Imoh").fullName() 

console.log("\nSoal 3")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
const {firstName, lastName, destination, occupation, spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

console.log("\nSoal 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log("\nSoal 5")
const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// Driver Code
const before = `Lorem ${view} dolor sit amet, `+
				`consectetur adipiscing elit, ${planet} `+
				`do eiusmod tempor incididunt ut labore et`+
				` dolore magna aliqua. Ut enim ad minim veniam`
console.log(before) 